/// <reference types="Cypress" />

class OportunidadesPage {

    get lista() { return cy.get('.cards > .card-flip') }

}

module.exports = new OportunidadesPage();