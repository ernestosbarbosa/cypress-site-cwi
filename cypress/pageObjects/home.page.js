/// <reference types="Cypress" />

class HomePage {

    get menu() { return cy.get("#main-header-menu .link") }

    clickMenu(item){
        this.menu.contains(item).click();
    }

}

module.exports = new HomePage();