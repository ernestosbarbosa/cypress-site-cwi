/// <reference types="Cypress" />
const home = require('../../pageObjects/home.page')
const oportunidades = require('../../pageObjects/oportunidades.page')

describe('Oportunidades', () => {

    beforeEach(() => {
        cy.visit("https://cwi.com.br/");
    })

    it('Validar o total de oportunidades', () => {
       home.clickMenu('Oportunidades');
       oportunidades.lista.should('have.length', 6);
    })

})